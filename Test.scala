import onebox._

object Test {
  def main(args: Array[String]) {
    println("\n\n=== START ONEBOX TESTS ===\n\n");

    println("Search")
    println(Onebox.search("pizza, austin"))

    println("\n\n");

    var addressList = List(
      "Bob's Sushi Restaurant",
      "123 Anywhere Street, New York, NY 10025",
      "PO Box 123456, New York, NY 10001",
      "321 W 4th Street, New York, NY 10001",
      "10025",
      "Park Avenue",
      "Park Avenue Winter"
    )

    var c: Int = 0
    addressList foreach(x => {
      println(x)
      println(Onebox.isLocation(x))
    })

    println("\n\n=== END ONEBOX TESTS ===\n\n");
  }
}
