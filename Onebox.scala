package onebox

case class Address(
  poBox: String,
  streetNumber: String,
  streetName: String,
  streetUnit: String,
  streetPrefixDir: String,
  streetPostfixDir: String,
  city: String,
  state: String,
  postalCode: String
)

object Onebox {
  val ADDRESS_KEYWORDS = List(
    "AVENUE","AVE","AV",
    "BOULEVARD","BLVD",
    "CIRCLE","CIR",
    "COURT","CT",
    "DRIVE","DR",
    "EXPRESSWAY","EXPWY","EXPY","EXP",
    "HIGHWAY","HWY",
    "LANE","LN",
    "PARKWAY","PKWY",
    "PLACE","PL",
    "ROAD","RD",
    "SQUARE","SQ",
    "STREET","ST",
    "WAY","WY",
    "AND","&" // could be cross streets
  )
  val DIRECTION_KEYWORDS = List(
    "NORTH","SOUTH","EAST","WEST",
    "N","S","E","W",
    "NW","SW","NE","SE"
  )

  def isNumeric(i: String): Boolean = i.forall(_.isDigit)

  def isLocation(a: String): Boolean = {
    var confidence:Int = 0
    val threshold:Int = 2
    // start by replacing periods, change to uppercase, split by commas and spaces
    val parts = List.fromArray(a.toUpperCase().replace(".","").split("[, ]+"))

    // PO box is a pretty sure sign. Probably only shows up with cut and paste though.
    if(parts(0) == "PO" && parts(1) == "BOX" && isNumeric(parts(2))) {
      confidence += threshold;
    }

    // contains one or more address keywords, numbers or directionals?
    parts.view.zipWithIndex foreach { case(p,i) => {
      if((!ADDRESS_KEYWORDS.find((s: String) => s == p).isEmpty) ||
         (!DIRECTION_KEYWORDS.find((s: String) => s == p).isEmpty) ||
         isNumeric(p) ) {
        confidence += 1
        // more confidence if the first or last word is an address keyword. Less if it isn't.
        if(i == 0) {
          confidence += 1;
        } else if(i == (parts.length-1)) {
          confidence += 1;
        }
      }
      // "Park Avenue" or "Prince St." are loations. "Prince St. Bakery" should not.
      if(parts.length == 2 && (!ADDRESS_KEYWORDS.find((s: String) => s == p).isEmpty)) {
        confidence += 1;
      }
    }}
    // is a 5-digit number by itself (probably a ZIP code)
    if(isNumeric(a) && a.length == 5) {
      confidence += threshold;
    }
    return confidence >= threshold
  }

  def getAddressParts(a: String): Address = {
    Address(
      "",
      "717",
      "Madison Ave",
      "3A",
      "",
      "",
      "New York",
      "NY",
      "10065"
    )
  }

  def getTermsFromMixedQuery(q: String): List[String] = {
    // returns a List (Name,Address)

    // the word "near" triggers a what/where search
    if(q.indexOf(" near ") != -1) {
      return List(q.split(" near ")(0), q.slice(q.indexOf(" near ")+6,q.length))
    }

    // otherwise divide at the first comma
    val parts = q.split("[,]+")
    List(parts(0), parts(1))
  }

  def search(query: String): String = {
    return getTermsFromMixedQuery(query).toString;
  }
}
